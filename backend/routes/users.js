
const admin = require('../middlewares/admin');
const bcrypt = require('bcrypt');
const express = require('express');
const router = express.Router();

const UserModel = require('../models/User');

router.get('/', async (req, res) => {

	const users = await UserModel.find(); 
	res.send(users);

});


router.get('/:id', async (req, res) => {
	const user = await UserModel.findById(req.params.id);
	res.send(user);
});

// router.post('/register',); assignment 06-20-19

router.post('/', async (req, res) => {

	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(req.body.password, salt)

	let user = await UserModel ({
		email: req.body.email,
		password: hashedPassword,
		name: {
			first: req.body.firstName,
			last:req.body.lastName,
			middle:req.body.middleName
		}, 
		type: 'user'

	});	
	user = await user.save();

	res.send(user);
});

router.put('/:id', async(req, res) => {

	// const hashedPassword = await bcrypt.hash(req.body.password, salt);
	let user = await UserModel.findByIdAndUpdate(req.params.id, {
		email: req.body.email,
		password: req.body.password,
		name: {
			first: req.body.firstName,
			last:req.body.lastName,
			middle:req.body.middleName
		}
		
	}, {new: true});	

	// task.taskName = req.body.task;


	res.send(user);
});



router.delete('/:id', async(req, res) => {
	let user = await UserModel.findByIdAndRemove(req.params.id);
	res.send(user);
});
	


module.exports = router;
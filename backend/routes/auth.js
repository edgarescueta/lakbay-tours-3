const jwt = require('jsonwebtoken');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const UserModel = require('../models/User');
const config = require('../config');

router.post('/', async(req, res) => {

	let user = await UserModel.findOne({ email:req.body.email });
	

	if(!user){
		 return res.status(400).send({
			message: "Email or Password is invalid"
		});
	}
	
	// console.log(req.body.password + user.password);
	let isMatch = await bcrypt.compare(req.body.password, user.password);
	
	if(!isMatch){
		return res.status(400).send({
			message: "Email or Password is invalid"
		});
	}



	const payload = {
		id: user._id, 
		email: user.email,
		fullname: `${user.name.first} ${user.name.middle} ${user.name.last}`,
		type: user.type
	};

	//generate token
	const token = jwt.sign( payload, config.secret );		// jwt.sign(payload, secret); more on (( https://jwt.io/ ))

	// attach the token to the header
	res.header('x-auth-token', token).send(user);

});




module.exports = router;
const admin = require('../middlewares/admin');
const auth = require('../middlewares/auth');
const express = require('express');
const mongoose = require('mongoose');
const router= express.Router();

const TaskModel = require('../models/Task');


router.get('/', async (req, res) => {
	let tasks = await TaskModel.find({});
	console.log(tasks);
	res.send(tasks);
});

router.post('/', [auth], async(req, res) => {
	//second argument is middleware. use [] to put two or more middleware
	let task = new TaskModel({
		taskName: req.body.task,
		user: req.user._id
	});

	task = await task.save();

	res.send(task);

});

router.put('/:taskId', async (req, res) => {
	
	let task = await TaskModel.findByIdAndUpdate(req.params.taskId, {
		taskName: req.body.task
	}, {new: true});	//third argument outputs the updated new value, default is false

	// task.taskName = req.body.task;
	// task = await task.save();

	res.send(task);
});


router.delete('/:taskId', async(req, res) => {
	let task = await TaskModel.findByIdAndRemove(req.params.taskId);
	res.send(task);
});


router.post('/:taskId/subtasks', async(req, res) => {
	//find
	let task = await TaskModel.findById(req.params.taskId);

	//create object subtask
	let subTask = {
		taskName: req.body.task,
	}

	//push the object to subTask in Schema model
	task.subTasks.push(subTask);
	// save

	task = await task.save();

	res.send(task);

});



module.exports = router;




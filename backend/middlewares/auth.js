const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = (req, res, next) => {
	const token = req.header('x-auth-token');

	if(!token) {
		return res.status(401).send("Access Denied. No token provided");
	}

	try {
		const payload = jwt.verify(token, config.secret);

		req.user = payload;		//user defined, attached payload to req. can be accessd by using req.user

		next();
	} catch	(ex){
		return res.status(400).send('Invalid Token');
	}
}
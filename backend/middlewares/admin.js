module.exports = (req, res, next) => {
	if(!req.user){
		return res.status(401).send('Unauthorized Access!');
	}

	if (req.user.type != "admin") {
		return res.status(403).send('Access Forbidden!');
	}

	next();
}
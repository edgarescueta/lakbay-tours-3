<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $client = new Client;
        $response = $client->get('localhost:3000/api/tasks');
        dd('sdf');

        $tasks = json_decode($response->getBody());

        //wrapping the $tasks array to a collection and reversing it in order
        $tasks = collect($tasks)->reverse();
        return view ('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //showing the value that we get/passes
        // dd($request->newtask);

        $client = new Client;
        $response = $client->post('localhost:3000/api/tasks', [
            "json" => [
                "task" => $request->newtask
            ],
            "header"=>[
                "x-auth-token" => Session::get('token')
            ]
        ]);


        //showing the decoded response that we get from the node
        // dd(json_decode($response->getBody()));

        return redirect('/');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = new Client;
        $client->put("localhost:3000/api/tasks/{$id}", [
            "json" => [
                "task" => $request->task
            ]
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = new Client;
        $client->delete("localhost:3000/api/tasks/{$id}");

        return redirect('/'); 

    }
}

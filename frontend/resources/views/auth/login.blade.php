@extends('layouts.app')


@section('content')
	<div class="container">
		@if(Session::has('invalidCredentials'))
			<div class="alert alert-danger" role="alert">
				{{ Session::get('invalidCredentials') }}
			</div>
		@endif
		<form action="/login" method="POST">
			@csrf
			<label for="Email">Email</label>
			<input type="text" class="form-control" name="email">
			<label for="password">Password</label>
			<input type="text" class="form-control" name="password">

			<button class="btn btn-primary">Login</button>
		</form>
		
	</div>
@endsection
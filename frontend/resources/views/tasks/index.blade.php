@extends('layouts.app')

@section('content')

<!-- <header class="masthead">

	<div class="container">

<div class="jumbotron">
<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 pull-right ">
                <img vspace="20" class="jumbotronwidth" alt=" " src="{{URL::asset('/img/logos/LakbayTours_logo-01.png')}}">
</div>
<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <h1>
                    Hello, world!
                </h1>

                <p>
                    This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.
                </p>
                <p>
                    <a class="btn btn-primary btn-large" href="#">Learn more</a>&nbsp;<a class="btn btn-primary btn-large" href="#">Extras</a>
                </p>
            </div></div>

      </div>
      
</header> -->

	 <!-- Header -->
  <header class="masthead">
    <div class="container">

      <div class="intro-text">
      	<img src="{{URL::asset('/img/logos/LakbayTours_logo-01.png')}}" style="width:400px"></img>
        <div class="intro-lead-in">Discover and book amazing things to do at exclusive prices</div>
        <div class="intro-heading text-uppercase">YOURS TO EXPLORE!</div>
        <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Tell Me More</a>
      </div>
    </div>

  </header>


 <!-- ROW 1 - FEATURES 3 COLUMNS ------------------- -->

    <div class="container">

			<div class="row">

					<div class="col-12 col-lg-4">

							<div class="card mb-3">

								  <h3 class="card-header">Boracay</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions.</p>
								  </div>

								  <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>
								
							</div>
					</div>

					<div class="col-12 col-lg-4">

							<div class="card mb-3">

								  <h3 class="card-header">Ilocos</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions.</p>
								  </div>

								  <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>
								  
							</div>

					</div>

					<div class="col-12 col-lg-4">

						    <div class="card mb-3">						    	

								  <h3 class="card-header">Batanes</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions..</p>
								  </div>
								 
								 <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>

							</div>
					</div>
			</div>

	</div> <!-- CONTAINER ------- -->


<!-- END OF ROW 1 - FEATURES 3 COLUMNS ------------------- -->

<!-- ROW 2 - FEATURES 3 COLUMNS ------------------- -->

    <div class="container">

			<div class="row">

					<div class="col-12 col-lg-4">

							<div class="card mb-3">

								  <h3 class="card-header">Coron</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions.</p>
								  </div>

								  <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>
								
							</div>
					</div>

					<div class="col-12 col-lg-4">

							<div class="card mb-3">

								  <h3 class="card-header">Siargao</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions.</p>
								  </div>

								  <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>
								  
							</div>

					</div>

					<div class="col-12 col-lg-4">

						    <div class="card mb-3">						    	

								  <h3 class="card-header">Camiguin</h3>
								  
								  <img style="height: 200px; width: 100%; display: block;" src="{{URL::asset('/img/boracay1.jpg')}}" alt="Card image">
								  <div class="card-body">
								    <p class="card-text">Discover Boracay's white-sandy beaches, beautiful waters, and great attractions..</p>
								  </div>
								 
								 <a class="btn btn-primary btn-lg" href="#" role="button">Book Now!</a>

							</div>
					</div>
			</div>

	</div> <!-- CONTAINER ------- -->


<!-- END OF ROW 2 - FEATURES 3 COLUMNS ------------------- -->



<!-- FOOTER SECTION -->


<footer class="page-footer font-small blue pt-4 bg-light">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <ul class="list-unstyled text-white">
              <li>
                <a href="#!">Home</a>
              </li>
              <li>
                <a href="#!">About</a>
              </li>
              <li>
                <a href="#!">Features</a>
              </li>
              <li>
                <a href="#!">Pricing</a>
              </li>
              <li>
                <a href="#!">Contact Us</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->

            <ul class="list-unstyled">
              <li>
                <a href="#!">Blog</a>
              </li>
              <li>
                <a href="#!">Search</a>
              </li>
              <li>
                <a href="#!">T & Cs</a>
              </li>
              <li>
                <a href="#!">Privacy</a>
              </li>
              <li>
                <a href="#!">Community</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">Get our newsletter</h5>

	            <form>
				  <div class="form-group">
				    <label for="exampleInputEmail1"></label>
				    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
				  </div>
				  
				  <button type="submit" class="btn btn-primary">Subscribe</button>
				</form>

        </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2019 Copyright:
      <a href="#">Edgar Escueta</a>
    </div>
    <!-- Copyright -->

  </footer>


  <!-- END OF FOOTER SECTION -->


@endsection
	
	<!-- <div class="card mx-auto my-2" style="width: 18rem;">
		<div class="card-header bg-dark text-light">
			Tasks
		</div>
		<div class="card-body">


			<form action="/" method="POST">
				@csrf
				<div class="form-group">
					{{-- <label for="addtask">Add Task</label> --}}
					<input type="text" name="newtask" class="form-control" id="addtask" placeholder="Add Task">
					<button type="submit" class="btn btn-primary btn-block my-1">Add</button>
				</div>
				
			</form>

			

		</div>
		<ul class="list-group list-group-flush">
			@forelse($tasks as $task)
			{{-- display item name --}}
			<li class="list-group-item">
				{{ $task->taskName }}

				<form action="/tasks/{{ $task->_id }}" method="POST">
					@csrf
					@method('PUT')
					<input type="text" name="task" class="form-control">
					<button class="btn btn-primary">Update</button>
					
				</form>

				<form action="/tasks/{{ $task->_id }}" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger">Delete</button>
					
				</form>

				
			</li>

			@forelse($task->subTasks as $subTask)
			<li class="list-group-item">-> {{ $subTask->taskName }}</li>

			@empty
			@endforelse


			@empty
			<h5>No item in list</h5>
			@endforelse
			{{-- {{ $tasks[0]->taskName }} --}}
		</ul>
	</div>
</div> -->

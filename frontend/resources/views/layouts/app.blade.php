<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Lakbay Tours - @yield('title')</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	  <!-- // BOOTSTRAP THEME  -->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	  <meta name="description" content="">
	  <meta name="author" content="">

	  <!-- Bootstrap core CSS -->
	  <link href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

	  <!-- Custom fonts for this template -->
	  <link href="{{ URL::asset('fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
	  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
	  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

	  <!-- Custom styles for this template -->
	  <!-- <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/agency.css') }}"> -->
	  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/agency.min.css') }}">
	  <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.css') }}">

</head>

<body>

	<body id="page-top">

		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<!-- <a class="navbar-brand" href="/">Lakbay Tours</a> -->
		<a  href="/"><img src="{{URL::asset('/img/logos/LakbayTours_logo-02.png')}}" style="width:200px"></img></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">

			<ul class="navbar-nav ml-auto">

				<!-- @if(Session::get('user')) -->
				<li class="nav-item active">
					<a class="nav-link" href="/">Lakbay Tours <span class="sr-only">(current)</span></a>
				</li>
				
				<li class="nav-item">
					<a href="/logout" class="nav-link">Logout</a>
				</li>

				<!-- @else -->
				<li class="nav-item">
					<a href="/login" class="nav-link">Login</a>
				</li>
				<li class="nav-item">
					<a href="/register" class="nav-link">Register</a>
				</li>

				<!-- @endif -->
				
			</ul>
		</div>
	</nav>

 

	@yield('content')


	<!-- Bootstrap core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	  <!-- BOOTSTRAP THEME -->
	  <script src="{{ URL::asset('/vendor/jquery/jquery.min.js') }}"></script>
	  <script src="{{ URL::asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

	  <!-- Plugin JavaScript -->
	  <script src="{{ URL::asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

	  <!-- Contact form JavaScript -->
	  <script src="{{ URL::asset('js/jqBootstrapValidation.js') }}"></script>
	  <script src="{{ URL::asset('js/contact_me.js') }}"></script>

	  <!-- Custom scripts for this template -->
	  <script src="{{ URL::asset('js/agency.min.js') }}"></script>


</body>
</html>